package util;

import java.io.File;
import java.io.IOException;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;

import play.Play;

public class ImageUtil {

  public static boolean isLandscape(File file) {
    BufferedImage image = null;
    try {
      image = ImageIO.read(file);
    } catch(IOException e) {
      return false;
    }
    int width           = image.getWidth();
    int height          = image.getHeight();
    return width > height;
  }

}