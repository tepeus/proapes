package util;

import java.io.File;
import java.io.IOException;

import play.Play;

import java.util.Date;

public class FileUtil {

  public static void createFile(File file) {
    try {
      file.createNewFile();
    } catch (IOException e) {
    }
  }

  public static void deleteFile(File file) {
    if(file.exists()) {
      file.delete();
    }
  }

  public static String addImage(File file, final String PATH, Date createdAt, String suffix, Long idModel){
    System.out.println("Created  = " + createdAt);
    String id       = DateUtil.dateOnlyWithFormat(createdAt, "yyyy-MM-dd-HH-mm-ss");
    String filename = String.format("%s-%s", suffix, id);
    String path     = String.format("%s/%d/", PATH, idModel);
    File newPath    = new File(path);
    if(!newPath.exists()) {
      newPath.mkdirs();
    }
    
    file.renameTo(new File(String.format("%s%s", path, filename)));
    FileUtil.createFile(file);
    return String.format("%d/%s", idModel, filename);
  }

  public static String newsPath() {
    return Play.application().getFile("public/images/news").getAbsolutePath();
  }
  
  public static String eventsPath() {
    return Play.application().getFile("public/images/events").getAbsolutePath();
  }
  
  public static String staffsPath() {
    return Play.application().getFile("public/images/staffs").getAbsolutePath();
  }

  public static String productPath() {
    return Play.application().getFile("public/images/products").getAbsolutePath();
  }

  public static String shopPath() {
    return Play.application().getFile("public/images/shops").getAbsolutePath();
  }
  
  public static String bandPath() {
    return Play.application().getFile("public/images/bands").getAbsolutePath();
  }
  
}
