package util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {

  public static Date dateAndHour(String date) {
    DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    try {
      return format.parse(date);
    } catch (ParseException e) {
      return null;
    }
  }
  
  public static Date date(String date) {
    DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
    try {
      return format.parse(date);
    } catch (ParseException e) {
      return null;
    }
  }
  
  public static String dayOfWeek(Date date) {
    Calendar c = Calendar.getInstance();
    c.setTime(date);
    int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
    switch(dayOfWeek) {
      case 1:
      return "Domingo";
      case 2:
      return "Segunda-feira";
      case 3:
      return "Terça-feira";
      case 4:
      return "Quarta-feira";
      case 5:
      return "Quinta-feira";
      case 6:
      return "Sexta-feira";
      case 7:
      return "Sábado";
    }
    return "" + dayOfWeek;
  }

  public static Date plusDays(int days) {
    Calendar c = Calendar.getInstance(); 
    c.setTime(new Date()); 
    c.add(Calendar.DATE, days);
    return c.getTime();
  }
  
  public static Date plusMonths(int months) {
    Calendar c = Calendar.getInstance(); 
    c.setTime(new Date()); 
    c.add(Calendar.MONTH, months);
    return c.getTime();
  }
  
  public static Date lastDayofMonth(int month, int year) {
      Calendar calendar = Calendar.getInstance();
      calendar.set(Calendar.MONTH, month-1);
      calendar.set(Calendar.YEAR, year);
      calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
      SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
      String date = format.format(calendar.getTime()); 
      try {
        format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return format.parse(String.format("%s 23:59:59", date));
      } catch (ParseException e) {
        return null;
      }
  }

  public static int currentMonth() {
    DateFormat format = new SimpleDateFormat("MM");
    String date = format.format(new Date());
    char positions[] = date.toCharArray();
    if(positions[0] =='0') {
      return Integer.parseInt(String.valueOf(positions[1]));
    }
    return Integer.parseInt(date);
  }
  
  public static int currentYear() {
    DateFormat format = new SimpleDateFormat("yyyy");
    return Integer.parseInt(format.format(new Date()));
  }
  
  public static String dateOnly(Date date) {
    return dateOnlyWithFormat(date, "dd/MM/yyyy");
  }
  
  public static String dateOnlyWithFormat(Date date, String format) {
    DateFormat formatter = new SimpleDateFormat(format);
    return formatter.format(date);
  }
  
  public static String hourOnly(Date date) {
	return dateOnlyWithFormat(date, "HH'h'mm");
  }
  
}
