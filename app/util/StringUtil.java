package util;

public class StringUtil {

  public static String abbreviation(String text, int amount) {
    String newText = "";
    if(text != null) {
      char letters[] = text.toCharArray();
      for (int i = 0; i < amount; i++) {
        if(i < text.length()) {
          newText += letters[i];
        } else {
          break;
        }
      }
    }
    return String.format("%s...", newText);
  }
  
  public static String convertToBr(String str) {
    if(str != null){
      return str.replaceAll("\n", "<br />");
    }else{
      return str;
    }
  }
}
