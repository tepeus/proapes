package util;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

public class NumberFormatHelper {
  
  public static String doubleToString(Double valor) {
    if (valor == null || valor == 0) {
      return "0";
    }
    DecimalFormat decimalFormat = new DecimalFormat ("#,##0.00", new DecimalFormatSymbols (new Locale ("pt", "BR")));
    return decimalFormat.format(valor);
  }
  
}
