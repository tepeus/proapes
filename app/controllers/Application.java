package controllers;

import java.util.ArrayList;
import java.util.List;

import models.User;
import play.data.DynamicForm;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.index;
import views.html.login;

public class Application extends Controller {

    public static Result index() {
        return ok(index.render());
    }

    public static Result login() {
    User user = Secured.currentUser();
    if(user == null){
  		return ok(login.render());
  	}
    	return redirect(routes.Application.index());
 	}
  
  
  	public static Result logout() {
    	session().clear();
    	return redirect(routes.Application.index());
  	}

  	public static Result authenticate() {
    	DynamicForm loginForm = Form.form().bindFromRequest();
    	if (loginForm.hasErrors()) {
       	   return badRequest(login.render());
    	}
  
    	User user = User.authenticate(loginForm.get("email"), loginForm.get("password"));
    	if(user == null) {
      		flash("error", "Login ou senha incorretos");
      		return badRequest(login.render());
    	}

    	session().clear();
    	session("email", user.email);
    	return redirect(routes.Application.index());
  }

}
