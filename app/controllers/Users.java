package controllers;

import models.User;
import play.data.DynamicForm;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;

public class Users extends Controller {
	
  final static Form<User> userform = Form.form(User.class);

  public static Result create() {
    return ok(views.html.users.create.render(userform));
  }
	
  @Security.Authenticated(Secured.class)
  public static Result edit(Long id) {
    User user = Secured.currentUser();
    Form<User> form = userform.fill(user);
    return ok(views.html.users.edit.render(form, id));
  }

  public static Result save() {
    
    Form<User> form = userform.bindFromRequest();
    if(form.hasErrors()) {
      return badRequest(views.html.users.create.render(form));
    }
       
    User user = form.get();
    String emailToFound = user.email;
    User userToFound = User.byEmail(emailToFound);
    if (userToFound != null) {
      flash("error","error");
      return badRequest(views.html.users.create.render(form));
    }

    user.save();
    
    flash("success","success");
    return found(routes.Users.create());
    
  }
  
  @Security.Authenticated(Secured.class)
  public static Result update(Long id) {
    
    Form<User> form = userform.bindFromRequest();

    if(form.hasErrors()){
      return badRequest(views.html.users.create.render(form));
    }
    
    User user = form.get();
    user.id = id;
    user.update();
    user.save();
    
    flash("success","success");
    return found(routes.Application.index());
    
  }

}  