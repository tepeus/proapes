package controllers;

import models.User;
import play.mvc.Http.Context;
import play.mvc.Result;
import play.mvc.Security;

public class Secured extends Security.Authenticator {

  public static final String SESSION_KEY = "email";
  public static final String TYPE_SESSION_KEY = "type";

  @Override
  public String getUsername(Context ctx) {
    return ctx.session().get(SESSION_KEY);
  }
  
  //@Override
  //public Result onUnauthorized(Context ctx) {
  //  return redirect(routes.Application.login());
  //}
    
  public static User currentUser() {
    Context ctx = Context.current();
    if (ctx.session().get(SESSION_KEY) == null) {
      return null;
    }
    
    final String email = ctx.session().get(SESSION_KEY);
    
    return User.find.where()
                    .eq("email", email)
                    .findUnique();
  }
  
}