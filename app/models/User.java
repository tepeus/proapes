package models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import play.data.validation.Constraints.Required;

@Entity
public class User extends Timestampable{

    @Required
	public String name;
	@Required
	public String email;
	@Required
	public String password;

	public User(String name,String email, String password){
		this.name = name;
		this.email = email;
		this.password = password;
	}

	public static Finder<Long,User> find = new Finder(Long.class, User.class);

	public static List<User> all() {
		return find.all();
	}

	public static User authenticate(String email,String password){
		return find.where()
                .eq("email", email)
                .eq("password",password)
                .findUnique();
	}

	public static User byEmail(String email){
		return find.where()
				   .eq("email", email)
				   .findUnique();
	}

	public static User byId(Long id) {
		return find.fetch("staff").where().eq("id",id).findUnique();
	}

}